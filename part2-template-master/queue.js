let collection = [];

// Write the queue functions below.
// (1) Make sure to have correct function names

function print() {
  return collection;
}

function enqueue(value) {
  collection.push(value);
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return collection.length == 0;
}
// (2) Make sure to include the functions in exportation
module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
